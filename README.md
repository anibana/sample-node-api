# MS using node (fp style)


### Running mongodb
```
docker-compose up --no-deps mongodb
```

### Running the application
```
npm run dev
```

```bash
docker-compose build
docker-compose up
```

or to build always
```bash
docker-compose up --build
```

### Testing
```bash
npm run test
```

To run tests in intellij/webstorm, update default mocha config by adding
```
Extra Mocha options: --compilers js:babel-core/register
```

Run single test in terminal

```bash
npm run test:only src/sample.test.js
```   
