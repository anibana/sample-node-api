#!/usr/bin/env bash

for opt in "$@"; do
  case ${opt} in
    --no-clean)
      DEV_NO_CLEAN=true
      ;;
    --build-only)
      DEV_BUILD_ONLY=true
      ;;
    --no-trap)
      DEV_NO_TRAP=true
      ;;
    --docker)
      DEV_BUILD_ONLY=true
      DEV_BUILD_TARGET='docker'
      ;;
  esac
done

echo "**********OPTIONS***********"
echo "no clean        : ${DEV_NO_CLEAN:-false}"
echo "build only      : ${DEV_BUILD_ONLY:-false}"
echo "build target    : ${DEV_BUILD_TARGET:-local}"
echo "****************************"

export PATH="$PATH:./node_modules/.bin"
export BABEL_ENV=development
export DEV_BUILD_TARGET=${DEV_BUILD_TARGET:-local}

DEV_BUILD_DIRECTORY=./dist

[ -z "$DEV_NO_CLEAN" ] && [ -d "$DEV_BUILD_DIRECTORY" ] \
  && echo "cleaning build directory" \
  && rm -rf ${DEV_BUILD_DIRECTORY} \
  && echo "directory ${DEV_BUILD_DIRECTORY} cleaned"

server=./dist/server/server.js

killWebpack() {
  echo "killing development webpack processes"
  webpackProcess=`ps aux | awk '/^.*[w]ebpack.*$/{print $2}'`
  [ -n "${webpackProcess}" ] && kill ${webpackProcess}
}

killClientDevelopment() {
  echo "killing client webpack process"
  clientProcess=`ps aux | awk '/^.*[c]lient.development.js.*$/{print $2}'`
  [ -n "${clientProcess}" ] && kill ${clientProcess}
}

if [ -z "$DEV_NO_TRAP" ]; then
  trap "killWebpack; exit" SIGHUP SIGINT SIGTERM
fi

killWebpack
killClientDevelopment

DEV_SERVER_WEBPACK_CONFIG_FILE=webpack/development/server.config.js

if [ -z "$DEV_BUILD_ONLY"  ]; then
  webpack --config ${DEV_SERVER_WEBPACK_CONFIG_FILE} --progress --watch &
else
  webpack --config ${DEV_SERVER_WEBPACK_CONFIG_FILE} --progress
fi


if [ -z "$DEV_BUILD_ONLY" ]; then
  while [ ! -f ${server} ]; do
    sleep 1
  done
  echo "now running node server, ${server} is now available"
  node --inspect=9222 ${server}
fi
