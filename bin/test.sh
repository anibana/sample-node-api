#!/usr/bin/env bash

TEST_MODE="test"
TEST_FILE=

for opt in "$@"; do
  case ${opt} in
    --mode=*)
      TEST_MODE=${opt#*=}
      ;;
    *)
      TEST_FILE="${TEST_FILE} ${opt}"
  esac
done

export PATH="$PATH:./node_modules/.bin"
export BABEL_ENV="test"

if [ "$TEST_MODE" == "cover" ]; then
  nyc --reporter=lcov --require babel-core/register mocha --require src/global.test.js --recursive "src/**/*.test.js"
  nyc report
elif [ "$TEST_MODE" == "watch" ]; then
  mocha --compilers js:babel-core/register --require src/global.test.js --recursive 'src/**/*.test.js' --watch
elif [ "$TEST_MODE" == "only" ]; then
  mocha --compilers js:babel-core/register --require src/global.test.js $TEST_FILE
else
  npm run lint && mocha --compilers js:babel-core/register --require src/global.test.js --recursive 'src/**/*.test.js'
fi
