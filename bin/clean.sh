#!/usr/bin/env bash

rm -rf .nyc_output
rm -rf node_modules
rm -rf coverage
rm -rf dist
