const nodeExternals = require('webpack-node-externals')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const path = require('path')
const webpack = require('webpack')

const isDocker = process.env['DEV_BUILD_TARGET'] === 'docker'

const entry = isDocker ?  './src/server/entry/development.js' : [
  'webpack/hot/poll?1000',
  './src/server/entry/development.js'
]

const plugins = [
  new webpack.IgnorePlugin(/\.(css|less)$/),
  new webpack.BannerPlugin({banner: 'require("source-map-support").install();', raw: true, entryOnly: false}),
  new webpack.HotModuleReplacementPlugin({quiet: true}),
  new webpack.NamedModulesPlugin(),
  new webpack.NoEmitOnErrorsPlugin()
]

const output = {
  path: path.resolve(__dirname, '../../dist/server/'),
  publicPath: '/dist/server',
  filename: 'server.js'
}

const devtool = 'inline-source-map'

const target = 'node'

const externals = [nodeExternals({whitelist: ['webpack/hot/poll?1000']})]

const buildModule = {
  rules: [
    {
      test: /.jsx?$/,
      loader: 'babel-loader',
      exclude: /node_modules/,
      options: {
        presets: [['env', {targets: {node: 'current'}}], 'stage-0'],
        plugins: [
          ['transform-object-rest-spread', {useBuiltIns: true}]
        ]
      }
    },
    {
      enforce: 'pre',
      test: /.js/,
      exclude: /node_modules/,
      loader: 'eslint-loader'
    }
  ]
}

const resolve = {
  alias: {
    fp: path.resolve(__dirname, '../../src/shared/fp.js')
  }
}

module.exports = {
  resolve,
  entry,
  plugins,
  output,
  devtool,
  target,
  externals,
  module: buildModule
}
