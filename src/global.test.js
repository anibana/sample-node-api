import chai, {expect} from 'chai'
import sinon from 'sinon'
import sinonChai from 'sinon-chai'
import dirtyChai from 'dirty-chai'
import {before, afterEach, after} from 'mocha'

chai.use(dirtyChai)
chai.use(sinonChai)

global.expect = expect
global.sinon = sinon

global.before = before
global.afterEach = afterEach
global.after = after

global.spy = sinon.spy
