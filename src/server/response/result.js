import {caseOf, identity} from 'fp'
import {badRequest, internalServerError, notFound, ok} from './response'
import {EventNotFound, matchError, UserNotFound, ValidationError} from '../errors'

export const futureResult = caseOf({
  success: identity,
  failure: internalServerError
})

export const errorMatch = matchError({
  [ValidationError.code]: badRequest,
  [EventNotFound.code]: notFound,
  [UserNotFound.code]: notFound,
  default: internalServerError
})

export const eitherResult = (matchError = errorMatch) => caseOf({
  right: ok,
  left: matchError
})

export const respondOkMsg = (message = 'ok') => () => ok(msg(message))

export const msg = (msg) => ({msg})
