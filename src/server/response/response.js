import HttpStatus from 'http-status-codes'
import send from 'koa-send'
import {REDIRECT, SENDFILE, STATUS} from './responseTypes'

// need to remove
export const sendFile = (root = '') => (file) => async (ctx) => {
  const result = await file({...ctx.query, ...ctx.params}, ctx.request.body)
  await send(ctx, result, {root})
}

const status = (status) => (body) => ({
  type: STATUS,
  status,
  body
})

export const internalServerError = status(HttpStatus.INTERNAL_SERVER_ERROR)
export const notFound = status(HttpStatus.NOT_FOUND)
export const unauthorized = status(HttpStatus.UNAUTHORIZED)
export const badRequest = status(HttpStatus.BAD_REQUEST)
export const ok = status(HttpStatus.OK)

export const sendFileOk = ({file, root = ''}) => ({file, root, type: SENDFILE})
export const redirect = ({url, cookies = {}}) => ({url, cookies, type: REDIRECT})
