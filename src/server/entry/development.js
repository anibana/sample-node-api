/* eslint-disable no-console,no-undef */
import server from '../server'
import '../middleware/development'
import '../config'
import '../dependencies'

const loadApp = (middleware, port) => {
  server.load(middleware, port)
  server.on('listening', () => {
    console.log(`server started on port ${port}`)
  })
}

const loadDeps = ([{default: middleware}, config, {default: dependencies}]) =>
  loadApp(middleware(dependencies(config)), config.port)

Promise.all([
  import('../middleware/development'),
  import('../config'),
  import('../dependencies')
]).then(loadDeps)
  .catch(err => console.log(err))

module.hot.accept()

