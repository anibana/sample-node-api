import repository from './repository'
//import httpClient from './httpClient'
import {middleware} from './middleware/middleware'

export default (config) => ({
  middleware: middleware(),
  repository: repository(config)
  //httpClient: httpClient(config)
})
