import {routes} from '../router/routes'
import {GET, POST, PUT, DELETE} from '../router/routeMethods'
import findById from './api/findById'
import create from './api/create'
import update from './api/update'
import deleteById from './api/deleteById'

export default routes([
  GET('/api/users/:id')(findById),
  POST('/api/users')(create),
  PUT('/api/users/:id')(update),
  DELETE('/api/users/:id')(deleteById)
])
