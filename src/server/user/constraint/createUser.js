import validator from '../../validation/validate'
import {firstName, lastName, email, status, password} from './fields'
import {shape} from '../../validation/validators/shape'
import {requiredFields} from '../../validation/helper'

export const constraint = requiredFields({
  firstName,
  lastName,
  email,
  status,
  password
})

export default validator(shape(constraint))
