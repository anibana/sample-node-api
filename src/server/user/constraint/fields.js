import {type, isObjectId, isValidEmail, inEnum} from '../../validation/validators'
import {types} from '../../../shared/fp'
import * as UserStatus from '../enum/UserStatus'

export const _id = [
  isObjectId
]

export const firstName = [
  type(types.String)
]

export const lastName = [
  type(types.String)
]

export const email = [
  isValidEmail
]

export const status = [
  inEnum(UserStatus)
]

export const password = [
  type(types.String)
]
