import validator from '../../validation/validate'
import {shape} from '../../validation/validators'
import {firstName, lastName, email, status, password} from './fields'

export const constraint = shape({
  firstName,
  lastName,
  email,
  status,
  password
})

export default validator(constraint)
