import * as base from '../db/base'
import composeRepository from '../db/composeRepository'

export default composeRepository(base)
