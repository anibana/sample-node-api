import Request from '../../request/request'
import {badRequest, notFound, ok} from '../../response/response'
import {findsUser, failsToFindUser} from './api.fixture.test'
import {UserNotFound, ValidationError} from '../../errors'
import findById from './findById'
import create from './create'
import {IS_REQUIRED} from '../../validation/validators/isRequired'
import {NEW} from '../enum/UserStatus'
import deleteById from './deleteById'
import update from './update'
import {INVALID_EMAIL} from '../../validation/validators/isValidEmail'

describe('user.api', () => {

  it('gets user', async () => {
    const request = Request({id: 'foo'})
    const deps = {repository: findsUser()}
    const result = await findById(request).run(deps).promise()
    expect(result).to.deep.equal(ok({id: 'foo'}))
  })

  it('does not get user', async () => {
    const request = Request({id: 'foo'})
    const deps = {repository: failsToFindUser()}
    const result = await findById(request).run(deps).promise()
    expect(result).to.deep.equal(notFound(UserNotFound()))
  })

  it('creates a user', async () => {
    const request = Request({}, {}, {
      email: 'foo@bar.baz',
      firstName: 'Foo',
      lastName: 'Bar',
      password: 'foobarbaz',
      status: NEW
    })
    const deps = {repository: findsUser()}
    const result = await create(request).run(deps).promise()
    expect(result).to.deep.equal(ok({id: 'foo'}))
  })

  it('fails to create an event when validation fails', async () => {
    const request = Request({}, {}, {
      firstName: 'Foo',
      lastName: 'Bar',
      password: 'foobarbaz',
      status: NEW
    })
    const deps = {repository: findsUser()}
    const result = await create(request).run(deps).promise()
    expect(result).to.deep.equal(badRequest(ValidationError({email: [IS_REQUIRED]},'validationerror')))
  })

  it('deletes a user', async () => {
    const request = Request({id: 'foo'})
    const deps = {repository: findsUser()}
    const result = await deleteById(request).run(deps).promise()
    expect(result).to.deep.equal(ok({msg: 'user deleted'}))
    expect(deps.repository.users.remove).has.been.calledWith({id: 'foo'})
  })

  it('updates a user', async () => {
    const request = Request({id: 'foo'}, {}, {email: 'foo@bar.baz'})
    const deps = {repository: findsUser()}
    const result = await update(request).run(deps).promise()
    expect(result).to.deep.equal(ok({msg: 'user updated'}))
    expect(deps.repository.users.update).has.been.calledWith({id: 'foo'}, {email: 'foo@bar.baz'})
  })

  it('does not update a user when updates are invalid', async () => {
    const request = Request({id: 'foo'}, {}, {email: 'invalidemail'})
    const deps = {repository: findsUser()}
    const result = await update(request).run(deps).promise()
    expect(result).to.deep.equal(badRequest(ValidationError({email: [INVALID_EMAIL]},'validationerror')))
  })

  it('does not update a user when user is not found', async () => {
    const request = Request({id: 'foo'}, {}, {email: 'foo@bar.baz'})
    const deps = {repository: failsToFindUser()}
    const result = await update(request).run(deps).promise()
    expect(result).to.deep.equal(notFound(UserNotFound('foo')))
  })
})
