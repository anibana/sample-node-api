import {compose, map, comap, askmap} from 'fp'
import {futureResult, respondOkMsg} from '../../response/result'

const deleteUserById = ({repository}) => ({params: {id}}) => repository.users.remove({id})

export default compose(
  map(futureResult),
  comap(respondOkMsg('user deleted')),
  askmap(deleteUserById)
)
