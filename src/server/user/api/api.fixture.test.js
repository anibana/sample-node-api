import {Future, Just} from '../../../shared/fp'
import {fakeRepo, repository} from '../../repository.fixture.test'

export const findsUser = () => repository({
  users: userFindsOne()
})

export const failsToFindUser = () => repository({
  users: fakeRepo()
})

const userFindsOne = () => ({
  findOne: ({id}) => Future.of(Just({id})),
  create: () => Future.of('foo')
})
