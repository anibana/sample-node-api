import {compose, comap, map, askmap, maybeToEither} from 'fp'
import {eitherResult, futureResult} from '../../response/result'
import {UserNotFound} from '../../errors'

const findUserById = ({repository}) => ({params: {id}}) => repository.users.findOne({id})

export default compose(
  map(futureResult),
  comap(eitherResult()),
  comap(maybeToEither(UserNotFound())),
  askmap(findUserById)
)
