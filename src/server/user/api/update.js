import {map, caseOf, futureLeft, compose, Right, comap, chain, askmap} from 'fp'
import {eitherResult, futureResult, msg} from '../../response/result'
import validateUpdate from '../constraint/updateUser'
import {maybeToEither} from '../../../shared/fp'
import {UserNotFound} from '../../errors'

const validateInput = ({params: {id}, body: updates}) =>
  map((updates) => [{id}, updates], validateUpdate(updates))

const findUser = ({repository}) => caseOf({
  right: checkUserExistence(repository),
  left: futureLeft
})

const checkUserExistence = (repository) => ([query, updates]) =>
  map(compose(maybeToEither(UserNotFound(query.id)), map(() => [query, updates])))(repository.users.findOne(query))

const updateWhenUserIsFound = ({repository}) => chain(caseOf({
  right: updateWhenUserExists(repository),
  left: futureLeft
}))

const updateWhenUserExists = (repository) => ([query, updates]) =>
  map(() => Right(msg('user updated')), repository.users.update(query, updates))

export default compose(
  map(futureResult),
  comap(eitherResult()),
  chain(askmap(updateWhenUserIsFound)),
  askmap(findUser),
  validateInput
)

