/* eslint-disable no-console,no-undef */
import {futureLeft, caseOf, map, comap, compose, askmap, Right} from 'fp'
import validateCreate from '../constraint/createUser'
import {eitherResult, futureResult} from '../../response/result'

const validateInput = ({body: user}) => validateCreate(user)

const saveIfValid = ({repository}) => caseOf({
  right: saveValidEvent(repository),
  left: futureLeft
})

const saveValidEvent = (repository) => (event) => {
  return map((id) => Right({id}), repository.users.create(event))
}

const trace = (tag) => (x) => {
  console.log(tag, x)
  return x
}

export default compose(
  map(futureResult),
  comap(eitherResult()),
  comap(trace('TEST')),
  askmap(saveIfValid),
  validateInput
)
