import route from './route'
import {fakeMiddleware} from '../router/routes.fixture.test'

describe('user.routes', () => {

  const router = route.run({
    middleware: fakeMiddleware()
  })

  it('has all the listed routes', async () => {
    expect(router.routes.get).to.have.all.keys(['/api/users/:id'])
    expect(router.routes.put).to.have.all.keys(['/api/users/:id'])
    expect(router.routes.delete).to.have.all.keys(['/api/users/:id'])
  })
})
