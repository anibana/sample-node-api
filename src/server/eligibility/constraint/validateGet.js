import validator from '../../validation/validate'
import {id, test} from './fields'
import {shape} from '../../validation/validators/shape'
import {requiredFields} from '../../validation/helper'

export const constraint = requiredFields({
  id,
  test
})

export default validator(shape(constraint))
