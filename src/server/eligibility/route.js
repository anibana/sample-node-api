import {routes} from '../router/routes'
import {GET} from '../router/routeMethods'
import test from './api/test'
import eligibility from './api/eligibility'

export default routes([
  GET('/api/eligibility')(eligibility)
])
