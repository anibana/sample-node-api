/* eslint-disable no-console,no-undef */
import {map, caseOf, futureLeft, compose, comap, askmap, Right, Future} from 'fp'
import {eitherResult, futureResult} from '../../response/result'
import validateGet from '../constraint/validateGet'
import {mapper} from '../mapper'

const validateRequest = () => ()

const getEligibility = ({httpClient}) => (request) => caseOf({
  right: icmCall(httpClient),
  left: futureLeft
})(request)

Future[Left Error]

const icmCall = (httpClient) => (request) =>
  map((v) => Right(mapper(v)), httpClient.getEligibility(request))

export default compose(
  map(futureResult),
  comap(eitherResult()),
  askmap(getEligibility),
  validateRequest
)

Request
validateRequest(Request) => Eihter[Error, Request]

askmmap(getEligibility)(Either[Error, Request]) => Reader[Future[Result]]
