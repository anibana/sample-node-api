import winston from 'winston'

export default new (winston.Logger)({
  level: process.env.logLevel || 'info',
  transports: [
    new (winston.transports.Console)({
      timestamp: true
    })
  ]
})
