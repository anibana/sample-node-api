import {Right, Left, caseOf} from '../../shared/fp'
import {ValidationError} from '../errors'

export default (constraints) => (value) => caseOf({
  just: ({errors, code}) => Left(ValidationError(errors, code)),
  nothing: () => Right(value)
})(constraints(value))
