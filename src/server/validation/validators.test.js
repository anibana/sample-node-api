import {Nothing, Just, types} from '../../shared/fp'
import {INVALID_TYPE, type} from './validators/type'
import {IS_REQUIRED, isRequired} from './validators/isRequired'
import {INVALID_OBJECT_ID, isObjectId} from './validators/isObjectId'
import {inEnum, NOT_IN_ENUM} from './validators/inEnum'
import {INVALID_EMAIL, isValidEmail} from './validators/isValidEmail'
import {INVALID_DATE, isValidDate} from './validators/isValidDate'
import {INVALID_TIME, isValidTime} from './validators/isValidTime'
import {INVALID_SHAPE, shape} from './validators/shape'

describe('validators', () => {
  it('returns Nothing if it is present', () => {
    expect(isRequired('hello')).to.eql(Nothing)
    expect(isRequired('')).to.eql(Nothing)
    expect(isRequired([])).to.eql(Nothing)
    expect(isRequired(0)).to.eql(Nothing)
  })

  it('returns Just of an empty value error on non present values', () => {
    expect(isRequired(null)).to.eql(Just(IS_REQUIRED))
  })

  it('fails on non ObjectIds', () => {
    expect(isObjectId('1')).to.eql(Just(INVALID_OBJECT_ID))
  })

  it('fails on value that is not in enums', () => {
    expect(inEnum({A: 'A', B: 'B'})('X')).to.eql(Just(NOT_IN_ENUM))
  })

  it('checks email validity', () => {
    expect(isValidEmail('test@gmail.com')).to.eql(Nothing)
    expect(isValidEmail('test')).to.eql(Just(INVALID_EMAIL))
  })

  it('checks date validity', () => {
    expect(isValidDate()('2010-01-01T05:06:07')).to.eql(Nothing)
    expect(isValidDate()('not a valid date')).to.eql(Just(INVALID_DATE))
  })

  it('checks time validity', () => {
    expect(isValidTime()('12:05')).to.eql(Nothing)
    expect(isValidTime()('not a valid time')).to.eql(Just(INVALID_TIME))
  })

  it('checks type', () => {
    expect(type(types.String)('bar')).to.eql(Nothing)
    expect(type(types.String)(123)).to.eql(Just(INVALID_TYPE))
  })

  it('checks shape', () => {
    const shapeOf = shape({
      foo: [],
      bar: [type(types.String)]
    })

    expect(shapeOf({foo: 'foo', bar: 'bar'})).to.eql(Nothing)

    expect(shapeOf({foo: 'foo', bar: 123})).to.eql(Just({
      code: INVALID_SHAPE,
      errors: {
        bar: [
          'INVALID_TYPE'
        ]
      }
    }))
  })

  it('checks deeply nested shapes', () => {
    const shapeOf = shape({
      foo: [
        shape({
          bar: [isRequired, type(types.String)]
        })
      ]
    })

    expect(shapeOf({foo: {bar: 'baz'}})).to.eql(Nothing)

    expect(shapeOf({foo: {bar: 123}})).to.eql(Just({
      code: INVALID_SHAPE,
      errors: {
        foo: [{
          code: INVALID_SHAPE,
          errors: {
            bar: [INVALID_TYPE]
          }
        }]
      }
    }))
  })

})
