import moment from 'moment'
import {matchesValidator} from './matchesValidator'

const isISODate = (date) => moment(date, moment.ISO_8601).isValid()

export const INVALID_DATE = 'INVALID_DATE'

export const isValidDate = (validators = [isISODate]) => matchesValidator(validators)(INVALID_DATE)
