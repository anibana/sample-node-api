import moment from 'moment'
import {matchesValidator} from './matchesValidator'

const isValidTimeFormat = (time) => moment(time, 'HH:mm').isValid()

export const INVALID_TIME = 'INVALID_TIME'

export const isValidTime = (validators = [isValidTimeFormat]) => matchesValidator(validators)(INVALID_TIME)
