import {typeOf} from '../../../shared/fp'
import {nothingWhenValid, nullable} from './helper'

export const INVALID_TYPE = 'INVALID_TYPE'

export const type = (type) => nullable(nothingWhenValid((value) => typeOf(value) !== type)(INVALID_TYPE))
