import {nothingWhenValid, nullable} from './helper'
import {find} from '../../../shared/fp'

export const NOT_IN_ENUM = 'NOT_IN_ENUM'

export const inEnum = (enums) =>
  nullable(nothingWhenValid((value) => !find((v) => value === v, Object.keys(enums)))(NOT_IN_ENUM))
