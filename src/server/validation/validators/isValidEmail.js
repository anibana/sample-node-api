import {regex} from './regex'

// eslint-disable-next-line no-useless-escape,max-len
const emailRegex = /^(([^<>()\[\].,;:\s@"]+(\.[^<>()\[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/i

export const INVALID_EMAIL = 'INVALID_EMAIL'

export const isValidEmail = regex(emailRegex)(INVALID_EMAIL)
