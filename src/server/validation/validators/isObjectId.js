import {regex} from './regex'

const objectIDRegex = /^[a-fA-F0-9]{24}$/

export const INVALID_OBJECT_ID = 'INVALID_OBJECT_ID'

export const isObjectId = regex(objectIDRegex)(INVALID_OBJECT_ID)
