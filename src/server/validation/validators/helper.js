import {chain, find, Just, Nothing, Maybe} from '../../../shared/fp'

export const nothingWhenValid = (condFn) => (err) => (value) =>
  condFn(value) ? Just(err) : Nothing

export const nullable = (fn) => (value) => chain(fn, Maybe.maybe(value))

export const findfn = (value) => find((fn) => fn(value))
