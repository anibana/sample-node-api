import {reduce, pairs, map, filter, isEmpty, Maybe, Nothing, Just} from '../../../shared/fp'
import {nullable} from './helper'

export const INVALID_SHAPE = 'INVALID_SHAPE'

const shapeOf = (shape) => {
  const validators = validateShape(shape)
  return (o) => {
    const validation = reduce(
      validateReducer(o),
      {},
      pairs(validators)
    )
    return isEmpty(validation) ? Nothing : Just({code: INVALID_SHAPE, errors: validation})
  }
}

const validateReducer = (o) => (acc, [prop, fn]) => {
  const validation = fn(o[prop], prop)
  if (isEmpty(validation)) {
    return acc
  }
  return {...acc, [prop]: map((e) => e.value, validation)}
}

const validateWith = (fns) => (value) =>
  filter(Maybe.isJust, map((fn) => fn(value), fns))

const validateShape = (constraint) => reduce(
  (acc, [prop, rules]) => ({...acc, [prop]: validateWith(map((rule) => rule, rules))}),
  {},
  pairs(constraint)
)

export const shape = (shape) => nullable(shapeOf(shape))
