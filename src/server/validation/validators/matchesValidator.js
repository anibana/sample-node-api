import {isNil} from 'fp'
import {findfn, nothingWhenValid, nullable} from './helper'

export const matchesValidator = (validators) => (err) =>
  nullable(nothingWhenValid((value) => isNil(findfn(value)(validators)))(err))
