import {nothingWhenValid, nullable} from './helper'
import {not} from '../../../shared/fp'

const valid = (regex) => (value) => regex.test(value)

export const regex = (regex) => (error) => nullable(nothingWhenValid(not(valid(regex)))(error))
