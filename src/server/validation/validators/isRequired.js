import {nothingWhenValid} from './helper'
import {isNil} from '../../../shared/fp'

export const IS_REQUIRED = 'IS_REQUIRED'

export const isRequired = nothingWhenValid(isNil)(IS_REQUIRED)
