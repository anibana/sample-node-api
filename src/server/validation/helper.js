import {isRequired} from './validators/isRequired'
import {pairs, reduce} from '../../shared/fp'

export const required = (constraints) => [isRequired, ...constraints]

export const requiredFields = (requiredFields) =>
  reduce((constraint, [key, value]) => ({...constraint, [key]: required(value)}), {}, pairs(requiredFields))
