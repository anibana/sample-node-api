import {type} from '../../validation/validators'
import {types} from '../../../shared/fp'

export const id = [
  type(types.String)
]

export const test = [
  type(types.String)
]
