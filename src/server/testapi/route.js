import {routes} from '../router/routes'
import {GET} from '../router/routeMethods'
import test from './api/test'

export default routes([
  GET('/api/test/:id')(test)
])
