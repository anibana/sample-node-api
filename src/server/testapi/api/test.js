/* eslint-disable no-console,no-undef */
import {map, caseOf, futureLeft, compose, comap, askmap, Right, Future} from 'fp'
import {eitherResult, futureResult} from '../../response/result'
import validateGet from '../constraint/validateGet'
import {mapper} from '../mapper'

const validateInput = ({params: {id}, query: {test}}) => validateGet({id, test})

const getValue = () => caseOf({
  right: (o) => map((v) => Right(mapper(v)), Future.of(o)),
  left: futureLeft
})

const inspect = (tag) => (x) => {
  console.log(tag, x)
  return x
}

//Reader future either result
export default compose(
  map(futureResult),
  comap(inspect('request')),
  comap(eitherResult()),
  askmap(getValue),
  validateInput
)
