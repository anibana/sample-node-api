import {Maybe, Future, reduce, toPairs} from 'fp'

export const repository = (repos = {}) => ({
  users: fakeRepo(),
  ...addRepos(toPairs(repos))
})

export const fakeRepo = (repo = {}) => reduce((r, [k, v]) => ({...r, [k]: sinon.spy(v)}), {}, toPairs({
  ...defaultRepo(),
  ...repo
}))

const addRepos = reduce((repos, [name, repo]) => ({...repos, [name]: fakeRepo(repo)}), {})

const defaultRepo = () => ({
  find: () => Future.of([]),
  findOne: () => Future.of(Maybe.nothing()),
  remove: () => Future.of(null),
  create: () => Future.of(null),
  update: () => Future.of(null)
})
