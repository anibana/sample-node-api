export const Request = ({params = {}, query = {}, body, headers = {}, cookies = {}}) => ({
  params,
  query,
  headers,
  cookies,
  body
})

export default (params, query, body, headers, cookies) => Request({
  params,
  query,
  body,
  headers,
  cookies
})
