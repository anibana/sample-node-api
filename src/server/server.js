import http from 'http'

// to make HMR working when reloading server side application

let server

const createServer = (middleware) => {
  if (!server) {
    server = http.createServer(middleware)
  }
}

const startServer = (middleware, port) => {
  createServer(middleware)
  server.listen(port)
}

const restartServer = (middleware, port) => {
  server.close(() => {
    server = null
    startServer(middleware, port)
  })
}

const load = (middleware, port) => {
  if (server) {
    return restartServer(middleware, port)
  }
  return startServer(middleware, port)
}

const on = (e, f) => server.on(e, f)

export default {
  load,
  on
}
