import Router from 'koa-router'

export default (f) => {
  const r = new Router()
  f(r)
  return r.routes()
}
