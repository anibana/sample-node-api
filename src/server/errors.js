const make = (code, fn) => {
  const error = function () {
    return {...fn.apply(null, arguments), code}
  }
  error.code = code
  return error
}

export const matchError = (cases) => (err) => (cases[err.code] || cases.default)(err)

export const ValidationError = make('validationerror', (errors, code) => ({
  msg: 'there are validation errors',
  code,
  errors
}))

export const UserNotFound = make('usernotfound', (id) => ({
  msg: `User with id ${id} is not found`
}))

export const UserAlreadyExists = make('useralreadyexits', (field, value) => ({
  msg: `User with ${field} ${value} already exists`
}))

export const EventNotFound = make('eventnotfound', (id) => ({
  msg: `Event with id ${id} not found`
}))

export const LoginRequired = make(
  'loginrequired',
  () => `login user required to perform the action`
)
