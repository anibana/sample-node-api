export default (db) => Promise.all([
  db.createIndex('users', {'place.coordinates': '2dsphere'}, {background: true})
])
