import {Reader, Maybe, map, chain, encaseP} from 'fp'

const toArray = encaseP((r) => r.toArray())

export const findOne = (query) =>
    Reader.ask.map((collection) => map(Maybe.maybe, collection.findOne(query)))

export const insertOne = (doc) =>
    Reader.ask.map((collection) => map((r) => r.insertedId, collection.insertOne(doc)))

export const find = (query = {}) =>
    Reader.ask.map((collection) => chain(toArray, collection.find(query)))

export const deleteOne = (query = {}) =>
    Reader.ask.map((collection) => collection.deleteOne(query))

export const updateOne = (query = {}, updates) =>
    Reader.ask.map((collection) => collection.updateOne(query, {$set: updates}))
