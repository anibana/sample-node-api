import {map, toPairs, compose, chain, reduce, Reader} from 'fp'

const mapPairsToProxy = (pairs) => Reader.ask.map((collection) => map(applyCollection(collection), pairs))

const applyCollection = (collection) => ([name, fn]) => [name, proxy(fn, collection)]

const proxy = (fn, collection) => function () {
    const args = [...arguments]
    return fn.apply(null, args).run(collection)
}

const reduceToRepository = reduce((repository, [name, fn]) => ({...repository, [name]: fn}), {})

export default compose(map(reduceToRepository), chain(mapPairsToProxy), map(toPairs), Reader.of)
