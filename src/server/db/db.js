import mongodb from 'mongodb'
import {Future} from 'fp'
import createIndex from './createIndex'

export default (config) => {
    const db = mongodb.MongoClient.connect(config.url, config.options)
      .then(db => createIndex(db).then(() => db))

    return {
        collection: (name) => {
            const proxy = collectionProxy(db.then(collectionOf(name)))
            return new Proxy({}, {
                get: (_, prop) => prop === 'collectionName' ? name : proxy(prop)
            })
        }
    }
}

const collectionProxy = (coll) => (prop) => function () {
    const args = [...arguments]
    return Future((reject, resolve) => {
        coll.then((coll) => coll[prop].apply(coll, args)).then(resolve).catch(reject)
    })
}

function collectionOf(name) {
    return (db) => new Promise((resolve, reject) => {
        db.collection(name, {}, (err, c) => err ? reject(err) : resolve(c))
    })
}
