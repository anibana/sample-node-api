import {map, comap} from 'fp'
import * as op from './operations'
import {idOf, _idOf} from './common'

export const findOne = (query) => map(comap(idOf), op.findOne(_idOf(query)))

export const create = (data) => op.insertOne(_idOf(data))

export const find = (query) => map(comap(idOf), op.find(query))

export const remove = (query) => op.deleteOne(_idOf(query))

export const update = (query, updates) => op.updateOne(_idOf(query), updates)
