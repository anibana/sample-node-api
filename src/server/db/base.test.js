import {Future, Maybe} from '../../shared/fp'
import * as base from './base'
import composeRepository from './composeRepository'
import {ObjectID} from 'mongodb'

describe('db.base', () => {

  const sandbox = sinon.sandbox.create()

  const id = '000000000000000000000000'
  const _id = ObjectID(id)

  const collection = {
    findOne: sandbox.spy(() => Future.of({_id: id})),
    insertOne: sandbox.spy(() => Future.of({insertedId: id})),
    find: sandbox.spy(() => Future.of({toArray: () => Promise.resolve([])})),
    deleteOne: sandbox.spy(() => Future.of(null)),
    updateOne: sandbox.spy(() => Future.of(null))
  }

  const repository = composeRepository(base).run(collection)

  afterEach(() => {
    sandbox.reset()
  })

  it('finds a data', async () => {
    const result = await repository.findOne({id}).promise()
    expect(collection.findOne).to.have.been.calledWith({_id})
    expect(result).to.deep.equal(Maybe.just({id}))
  })

  it('creates a data', async () => {
    await repository.create({id}).promise()
    expect(collection.insertOne).to.have.been.calledWith({_id})
  })

  it('finds all data', async () => {
    await repository.find().promise()
    expect(collection.find).to.have.been.calledWith({})
  })

  it('removes a data', async () => {
    await repository.remove({id}).promise()
    expect(collection.deleteOne).to.have.been.calledWith({_id})
  })

  it('updates a data', async () => {
    await repository.update({id}, {name: 'bar'}).promise()
    expect(collection.updateOne).to.have.been.calledWith({_id}, {$set: {name: 'bar'}})
  })
})
