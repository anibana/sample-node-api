import {reduce, toPairs, isNil} from 'fp'
import {ObjectID} from 'mongodb'

export const idOf = ({_id, ...rest}) => allDefined({id: _id, ...rest})

export const _idOf = ({id, ...rest}) => allDefined({_id: id ? ObjectID(id) : id, ...rest})

export const allDefined = (query) =>
    reduce((query, [key, value]) => value !== undefined ? {...query, [key]: value} : query, {}, toPairs(query))

const dateGte = (iso) => !isNil(iso) ? {$gte: new Date(iso)} : undefined

const dateLte = (iso) => !isNil(iso) ? {$lte: new Date(iso)} : undefined

export const date = {
  gte: dateGte,
  lte: dateLte,
  range: (from, to) => !isNil(from) || !isNil(to) ? {...dateGte(from), ...dateLte(to)} : undefined
}

export const near = ({lat, lng}) => ({
  $near: {
    $geometry: {type: 'Point', coordinates: [lng, lat]},
    $maxDistance: 2000
  }
})
