export const port = process.env.port || 3000
export const host = process.env.host || '0.0.0.0'
export const mongo = {
  url: process.env.MONGO_URL || 'mongodb://localhost:27017/barycenter'
}
export const mailApiKey = process.env.MAIL_API_KEY
export const auth = {
  google: {
    id: '933116173523-ahn9n2srlo3og45smqvdq258rsjb529s.apps.googleusercontent.com',
    secret: 'eAxKuFGf1Zj1-gXIM-nfeirx',
    redirectURL: 'http://localhost:3000/socialauth/complete/google'
  }
}
