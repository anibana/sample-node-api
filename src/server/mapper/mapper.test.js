import {asDate, asIs, mapper} from './index'

describe('mappers', () => {
  it('maps values as is', () => {
    const maps = mapper({a: asIs})

    expect(maps({a: 123})).to.eql({a: 123})
    expect(maps({a: 'Value'})).to.eql({a: 'Value'})
  })

  it('maps string as Date', () => {
    const maps = mapper({a: asDate})
    const mappedValue = maps({a: '2017-10-31 21:47:11.208Z'})

    expect((mappedValue.a).constructor).to.eql(Date)
  })
})
