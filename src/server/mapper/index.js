import {identity, pairs, reduce} from 'fp'

export const asIs = identity

export const asDate = (d) => new Date(d)

export const mapper = (propsMapper) => (object) =>
  reduce((o, [key, mapper]) => ({...o, [key]: mapper(object[key])}), {}, pairs(propsMapper))
