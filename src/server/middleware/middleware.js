import Router from 'koa-router'
import send from 'koa-send'
import {Request} from '../request/request'
import {REDIRECT, SENDFILE, STATUS} from '../response/responseTypes'
import {comap} from 'fp'
import HttpStatus from 'http-status-codes'

export const middleware = () => ({
  createRouter
})

export const createRouter = (dependencies) => {
  const r = new Router()
  const respond = createResponse(dependencies)
  const register = (route) => r[route.method](route.path, respond(route.api))
  return {
    register,
    routes: () => r.routes()
  }
}
export const createResponse = (dependencies) => (reader) => (ctx) =>
  comap(
    handleResponse(ctx),
    reader(request(ctx))
  ).run(dependencies).promise()

const request = (ctx) => Request({
  params: ctx.params,
  query: ctx.query,
  body: ctx.request.body,
  headers: ctx.request.headers
})

const handleResponse = (ctx) => (response) => {
  return responseHandlers[response.type](ctx)(response)
}

const handleStatus = (ctx) => ({status, body}) => {
  ctx.status = status
  if (body) {
    ctx.body = body
  }
}

const handleRedirect = (ctx) => ({url}) => ctx.redirect(url)

const handleSendFile = (ctx) => ({file, root = ''}) => {
  ctx.status = HttpStatus.OK
  return send(ctx, file, {root})
}

const responseHandlers = {
  [STATUS]: handleStatus,
  [REDIRECT]: handleRedirect,
  [SENDFILE]: handleSendFile
}
