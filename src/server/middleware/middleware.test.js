import {Reader, Future, noop} from 'fp'
import {createResponse} from './middleware'
import {ok, redirect, sendFileOk} from '../response/response'

describe('koa middleware', () => {

  const response = createResponse({})

  it('responds to status ok', async () => {
    const ctx = createCtx()
    await response(statusOk)(ctx)
    expect(ctx.body).to.equal('foo')
    expect(ctx.status).to.equal(200)
  })

  it('responds to redirect', async () => {
    const ctx = createCtx()
    await response(redirectUrl)(ctx)
    expect(ctx.redirect).to.have.been.calledWith('/')
    expect(ctx.cookies.set).to.have.been.calledWith('foo', 'bar', {})
  })

  it('responds to sendFile', async () => {
    const ctx = createCtx()
    await response(performSend)(ctx)
    expect(ctx.status).to.equal(200)
    expect(ctx.acceptsEncodings).to.have.been.called()
  })

  const statusOk = () => Reader.of(Future.of(ok('foo')))

  const redirectUrl = () => Reader.of(Future.of(redirect({url: '/', cookies: {foo: {value: 'bar'}}})))

  const performSend = () => Reader.of(Future.of(sendFileOk({file: '/'})))

  const createCtx = () => ({
    request: {headers: {}},
    redirect: spy(noop),
    cookies: {
      set: spy(noop)
    },
    acceptsEncodings: spy(noop)
  })
})
