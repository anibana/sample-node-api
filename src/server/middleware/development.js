import Koa from 'koa'
import bodyparser from 'koa-bodyparser'
import cors from 'kcors'
import morgan from 'koa-morgan'
import user from '../user/route'
import testapi from '../testapi/route'

export default (dependencies) => {
  const app = new Koa()

  app.use(morgan(':date[iso] :method :status :url :response-time ms - :res[content-length]'))
  app.use(bodyparser())
  app.use(cors())
  app.use(user.run(dependencies).routes())
  app.use(testapi.run(dependencies).routes())

  return app.callback()
}
