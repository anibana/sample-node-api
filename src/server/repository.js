import {reduce, toPairs} from 'fp'
import db from './db/db'
import users from './user/repository'

export default (config) => reduce(reduceRepository(db(config.mongo)), {}, toPairs({
  users
}))

const reduceRepository = (db) => (repos, [name, repo]) => ({...repos, [name]: repo.run(db.collection(name))})
