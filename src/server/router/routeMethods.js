const method = (method) => (path) => (api) => ({
    method,
    path,
    api
})

export const GET = method('get')

export const POST = method('post')

export const DELETE = method('delete')

export const PUT = method('put')
