export const fakeMiddleware = () => ({
    createRouter: (dependencies) => fakeRouter(dependencies)
})

export const fakeRouter = (dependencies) => {
    const routes = {
        get: {},
        post: {},
        put: {},
        delete: {},
        patch: {}
    }
    const evaluatable = (route) => (request) => route.api(request).run(dependencies).promise()
    return {
        register: (route) => routes[route.method][route.path] = evaluatable(route),
        routes
    }
}
