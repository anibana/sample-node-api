import {compose, chain, map, Reader, Future} from '../../shared/fp'
import {routes} from './routes'
import {GET} from './routeMethods'
import Request from '../request/request'
import {ok} from '../response/response'
import {fakeMiddleware} from './routes.fixture.test'

describe('routes', () => {

  it('creates a router that can map path and params', async () => {
    const m = fakeMiddleware()
    const router = routes([GET('/')(getStuff)]).run({middleware: m, hello: (text) => `hello${text}`})
    expect(router.routes.get['/']).to.exist()
    expect(await router.routes.get['/'](Request({id: 'world'}))).to.deep.equal(ok('helloworld'))
  })

  const perform = ({params: {id}}) => Reader.ask.map(({hello}) => hello(id))

  const getStuff = compose(
    map(compose(map(ok), Future.of)),
    chain(perform),
    Reader.of
  )

  //Reader(Request)
  //Reader(helloworld)
  //Reader(Future(ok(helloword)))
})
