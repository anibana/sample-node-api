import {reduce, askmap} from 'fp'

const register = (router, route) => {
  router.register(route)
  return router
}

const askRouter = (dependencies) => (routes) =>
  reduce(register, dependencies.middleware.createRouter(dependencies), routes)

export const routes = askmap(askRouter)
