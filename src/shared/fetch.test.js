import fetcher, {buildUrl} from './fetch'
import {Get, Delete, Patch, Post, Put} from './fetch/method'

describe('fetch wrapper', () => {

  const sandbox = sinon.sandbox.create()
  const fetchSpy = sandbox.spy(() => Promise.resolve())
  let fetch

  before(() => {
    fetch = fetcher(fetchSpy)
  })

  afterEach(() => {
    sandbox.reset()
  })

  after(() => {
    sandbox.restore()
  })

  it('gets', async () => {
    await fetch(Get()('/hello/world'))
    expect(fetchSpy).to.have.been.calledWith('/hello/world', {
      headers: {},
      method: 'GET'
    })
  })

  it('gets with parameters', async () => {
    await fetch(Get({query: {say: 'hi'}})('/hello/world'))
    expect(fetchSpy).to.have.been.calledWith('/hello/world?say=hi', {
      headers: {},
      method: 'GET'
    })
  })

  it('gets with headers', async () => {
    await fetch(Get({query: {say: 'hi'}, headers: {head: 'hunter'}})('/hello/world'))
    expect(fetchSpy).to.have.been.calledWith('/hello/world?say=hi', {
      headers: {head: 'hunter'},
      method: 'GET'
    })
  })

  it('posts', async () => {
    await fetch(Post()('/post'))
    expect(fetchSpy).to.have.been.calledWith('/post', {
      headers: {},
      method: 'POST'
    })
  })

  it('posts with json body', async () => {
    await fetch(Post()('/post', {please: 'post'}))
    expect(fetchSpy).to.have.been.calledWith('/post', {
      body: JSON.stringify({please: 'post'}),
      headers: {'content-type': 'application/json'},
      method: 'POST'
    })
  })

  it('posts with string body', async () => {
    await fetch(Post()('/post', 'please post'))
    expect(fetchSpy).to.have.been.calledWith('/post', {
      body: 'please post',
      headers: {'content-type': 'text/plain'},
      method: 'POST'
    })
  })

  it('posts with string body with explicit content type', async () => {
    await fetch(Post({headers: {'content-type': 'application/json'}})('/post', JSON.stringify({please: 'post'})))
    expect(fetchSpy).to.have.been.calledWith('/post', {
      body: JSON.stringify({please: 'post'}),
      headers: {'content-type': 'application/json'},
      method: 'POST'
    })
  })

  it('puts', async () => {
    await fetch(Put()('/put', {please: 'put'}))
    expect(fetchSpy).to.have.been.calledWith('/put', {
      body: JSON.stringify({please: 'put'}),
      headers: {'content-type': 'application/json'},
      method: 'PUT'
    })
  })

  it('patches', async () => {
    await fetch(Patch()('/patch', {please: 'patch'}))
    expect(fetchSpy).to.have.been.calledWith('/patch', {
      body: JSON.stringify({please: 'patch'}),
      headers: {'content-type': 'application/json'},
      method: 'PATCH'
    })
  })

  it('deletes', async () => {
    await fetch(Delete()('/delete'))
    expect(fetchSpy).to.have.been.calledWith('/delete', {
      headers: {},
      method: 'DELETE'
    })
  })

  it('builds url', () => {
    expect(buildUrl('/foo', {bar: 'baz'})).to.equal('/foo?bar=baz')
  })
})
