import queryString from 'querystring'
import {isEmpty, isNil} from '../fp'

const headers = (opts) => {
  if (isEmpty(opts.headers) && isNil(body)) {
    return {}
  }
  const headers = opts.headers || {}
  if (opts.body && !headers['content-type']) {
    headers['content-type'] = getType(opts.body)
  }
  return {headers}
}

const getType = (body) => {
  const bodyType = typeof body
  if (bodyType === 'string') {
    return 'text/plain'
  }
  if (bodyType === 'object') {
    return 'application/json'
  }
  return undefined
}

const body = (body) => {
  if (!isNil(body)) {
    if (typeof body === 'object') {
      return {body: JSON.stringify(body)}
    }
    return {body}
  }
  return undefined
}

export const buildUrl = (path, queryParams) => path + buildQuery(queryParams)

const buildQuery = (queryParams) => {
  return cleanUrl('?' + queryString.stringify(queryParams))
}

const cleanUrl = (url) => {
  if (url.endsWith('&') || url.endsWith('?')) {
    return url.substring(0, url.length - 1)
  }
  return url
}

export default (fetch) => (opts) => fetch(opts.url, {
  ...body(opts.body),
  ...headers(opts),
  ...opts.opts,
  method: opts.method
})
