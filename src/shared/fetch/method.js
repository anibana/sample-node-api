import {buildUrl} from './index'

const method = (method) => (options = {}) => function () {
  const {query = {}, headers = {}, opts = {}} = options
  return {
    url: buildUrl(arguments[0], query),
    body: arguments[1],
    method,
    headers,
    opts
  }
}

export const Get = method('GET')

export const Post = method('POST')

export const Put = method('PUT')

export const Patch = method('PATCH')

export const Delete = method('DELETE')

