/* eslint-disable no-console,no-undef */
import {Maybe, Either} from 'tsmonad'
import {Future} from 'fluture'
import {isNil, compose, map, toPairs} from 'ramda'
import Reader from 'fantasy-readers'

export Reader from 'fantasy-readers'

export {
  parallel,
  Future,
  encaseN,
  encaseN2,
  encaseN3,
  encaseP,
  encaseP2
} from 'fluture'

export {
  compose,
  map,
  chain,
  reduce,
  toPairs,
  identity,
  filter,
  find,
  pipe,
  pick,
  forEach,
  isNil,
  isEmpty,
  omit
} from 'ramda'

export {Maybe, Either} from 'tsmonad'

const maybeCase = (cases) => (m) => {
  if (Maybe.isJust(m)) {
    return cases.just(m.value)
  }
  return cases.nothing()
}

const eitherCase = (cases) => (e) => {
  if (!isNil(e.l)) {
    return cases.left(e.l)
  }
  return cases.right(e.r)
}

const futureCase = (cases) => (f) => f.fold(cases.failure, cases.success)

const monadCases = {
  Maybe: maybeCase,
  Either: eitherCase,
  Future: futureCase
}

export const caseOf = (cases) => (m) => monadCases[typeOf(m)](cases)(m)

export const types = {
  Maybe: 'Maybe',
  Either: 'Either',
  Future: 'Future',
  String: 'string',
  Object: 'object',
  Array: 'array',
  Number: 'number'
}

export const typeOf = (o) => {
  const type = typeof o
  if (type === 'object') {
    if (isEither(o)) {
      return types.Either
    }
    if (isMaybe(o)) {
      return types.Maybe
    }
    if (isFuture(o)) {
      return types.Future
    }
    if (o.constructor === Array) {
      return types.Array
    }
    return type
  }
  return type
}

export const nothingFuture = () => Future.of(Maybe.nothing())

export const isFuture = (o) => typeof o.promise === 'function'

const _comap = compose(map, map)

export const comap = function () {
  const args = [...arguments]
  return args.length > 1 ? _comap(args[0])(args[1]) : _comap(args[0])
}

export const head = (result) => Maybe.maybe(result[0])

export const Left = Either.left

export const Right = Either.right

export const Nothing = Maybe.nothing()

export const Just = Maybe.just

export const pairs = toPairs

export const not = (fn) => (v) => !fn(v)

export const maybeToEither = (nothing) => caseOf({
  just: Right,
  nothing: () => Left(nothing)
})

export const ask = Reader.ask

export const askmap = (m) => (v) => ask.map((a) => m(a)(v))

export const futureLeft = compose(Future.of, Either.left)

export const futureNothing = () => Future.of(Nothing)

const isMaybe = (o) => !!o.valueOr

const isEither = (o) => o.l !== undefined || o.r !== undefined
