import {typeOf, Just, Left, types, isEmpty} from './fp'

describe('fp', () => {
  it('identifies a Maybe correctly', () => {
    expect(typeOf(Just(1))).to.equal(types.Maybe)
  })

  it('identifies an Either correctly', () => {
    expect(typeOf(Left(1))).to.equal(types.Either)
  })

  it('identifies an object correctly', () => {
    expect(typeOf({})).to.equal('object')
  })

  it('checks if an object or array is empty', () => {
    expect(isEmpty({})).to.be.true()
    expect(isEmpty([])).to.be.true()
  })
})
